 
 import static java.lang.Math.PI;

 import static java.lang.Math.E;

 import static java.lang.Math.pow;

 import static java.lang.Math.sqrt;

 

 /**

  * UTFPR - Universidade Tecnológica Federal do Paraná

  * DAINF - Departamento Acadêmico de Informática

  * 

- * Template de projeto de programa Java usando Maven.

+ * Programa Java usando Maven.

  * @author Carolina Lima <carolinalima@alunos.utfpr.edu.br>

  */

 

 public class Pratica32 {

     

     public static double densidade(double x, double media, 

             double dp) {

 

         double d = ((1/(sqrt(2*(PI))*dp))*(pow(E,(-0.5 * pow(((x-media)/dp),2)))));

         return d;

     }

     public static void main(String[] args) {

         double d;

         double x = -1;

         double media = 67;

         double dp = 3;

         d = densidade(x,media,dp);

         System.out.print(d);

     }

 }
